﻿using HarmonyLib;
using Verse.AI;

namespace NMeijer.ConfigurableMentalBreaks
{
	[HarmonyPatch(typeof(MentalBreakWorker), nameof(MentalBreakWorker.BreakCanOccur))]
	public class Patch_MentalBreakWorker_BreakCanOccur
	{
		#region Private Methods

		private static void Postfix(ref bool __result, ref MentalBreakWorker __instance)
		{
			__result = __result && ConfigurableMentalBreaksMod.Settings.IsMentalBreakEnabled(__instance.def);
		}

		#endregion
	}
}