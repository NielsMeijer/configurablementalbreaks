﻿using System.Collections.Generic;
using UnityEngine;
using Verse;

namespace NMeijer.ConfigurableMentalBreaks
{
	public class ConfigurableMentalBreaksSettings : ModSettings
	{
		#region Variables

		private HashSet<string> _disabledMentalBreaks = new HashSet<string>();

		private Vector2 _scrollPosition;
		private Rect _viewRect;

		#endregion

		#region Public Methods

		public bool IsMentalBreakEnabled(MentalBreakDef mentalBreakDef)
		{
			return !_disabledMentalBreaks.Contains(mentalBreakDef.defName);
		}

		public override void ExposeData()
		{
			Scribe_Collections.Look(ref _disabledMentalBreaks, "disabledMentalBreaks");
			if(_disabledMentalBreaks == null)
			{
				_disabledMentalBreaks = new HashSet<string>();
			}

			base.ExposeData();
		}

		public void DoSettingsWindowContents(Rect inRect)
		{
			List<MentalBreakDef> mentalBreakDefs = DefDatabase<MentalBreakDef>.AllDefsListForReading;

			Listing_Standard listingStandard = new Listing_Standard();

			Widgets.BeginScrollView(inRect, ref _scrollPosition, _viewRect);
			inRect.height = 100000f;
			inRect.width -= 20f;
			listingStandard.Begin(inRect.AtZero());
			for(int i = 0, count = mentalBreakDefs.Count; i < count; i++)
			{
				MentalBreakDef mentalBreakDef = mentalBreakDefs[i];
				bool value = !_disabledMentalBreaks.Contains(mentalBreakDef.defName);
				listingStandard.CheckboxLabeled(mentalBreakDef.defName, ref value);
				if(value)
				{
					_disabledMentalBreaks.Remove(mentalBreakDef.defName);
				}
				else
				{
					_disabledMentalBreaks.Add(mentalBreakDef.defName);
				}
			}
			_viewRect = new Rect(0f, 0f, listingStandard.ColumnWidth, listingStandard.CurHeight);
			Widgets.EndScrollView();
			listingStandard.End();
		}

		#endregion
	}
}