﻿using HarmonyLib;
using UnityEngine;
using Verse;

namespace NMeijer.ConfigurableMentalBreaks
{
	public class ConfigurableMentalBreaksMod : Mod
	{
		#region Properties

		public static ConfigurableMentalBreaksSettings Settings
		{
			get; private set;
		}

		#endregion

		public ConfigurableMentalBreaksMod(ModContentPack modContentPack)
			: base(modContentPack)
		{
			Harmony harmony = new Harmony("nl.nmeijer.configurablementalbreaks");
			harmony.PatchAll();

			Settings = GetSettings<ConfigurableMentalBreaksSettings>();
		}

		public override void DoSettingsWindowContents(Rect inRect)
		{
			Settings.DoSettingsWindowContents(inRect);

			base.DoSettingsWindowContents(inRect);
		}

		public override string SettingsCategory()
		{
			return "Configurable Mental Breaks";
		}
	}
}