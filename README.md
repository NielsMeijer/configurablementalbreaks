# Features
Sick of colonists going crazy and destroying your very expensive research bench or going after your favorite animal? This is the mod for you!

With Configurable Mental Breaks you can disable any mental break you don't want.

Note: By default all mental breaks are enabled, don't forget to go into the mod settings to disable the ones you don't want.

# License
This mod is licensed under the [MIT license](https://tldrlegal.com/license/mit-license).

# Versions
## 1.0.4
* Updated to RimWorld v1.5

## 1.0.3
* Updated to RimWorld v1.4

## 1.0.2
* Updated to RimWorld v1.3

## 1.0.1
* Updated to RimWorld v1.2

## 1.0.0
* Initial release